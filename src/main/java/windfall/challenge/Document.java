package windfall.challenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Immutable document representing a CSV data file
 */
public class Document {

    private final List<String[]> rows;

    public Document(List<String[]> rows) {
        if (rows.size() == 0) {
            throw new IllegalArgumentException("Invalid document: no data");
        }

        int width = rows.get(0).length;
        if (rows.stream().anyMatch(row -> row.length != width)) {
            throw new IllegalArgumentException("Invalid document: all columns must have same width");
        }

        if (width > 26) {
            throw new IllegalArgumentException("Sorry, this version only supports up to 26 columns (from A to Z)");
        }

        this.rows = Collections.unmodifiableList(rows);
    }

    /**
     * Retrieves the cell at the specified coordinate (in the form COLROW)
     */
    public String getCellAt(String coordinate) {
        String[] coord = parseCoordinate(coordinate.toUpperCase());
        char column = coord[0].charAt(0);
        int row = Integer.parseInt(coord[1]);

        if (column < 'A' || column > 'Z') {
            throw new IllegalArgumentException("Column is not within A-Z range: " + coordinate);
        }

        if (row <= 0) {
            throw new IllegalArgumentException("Row must be in the range of 1 to " + rows.size());
        }

        int columnIndex = column - 'A';
        String[] cells = rows.get(row - 1);
        if (row > rows.size() || columnIndex < 0 || columnIndex >= cells.length) {
            throw new IllegalArgumentException("Cell out of bounds: " + coordinate);
        }

        return cells[columnIndex];
    }

    public int getHeight() {
        return rows.get(0).length;
    }

    public char getWidth() {
        return (char) ('A' + rows.size() - 1);
    }

    /**
     * Creates a new document by mapping every cell and its value in
     * this document to the output of the provided function.
     */
    public Document map(BiFunction<String, String, String> mapCell) {
        List<String[]> cols = new ArrayList<>();
        for (int row = 1; row <= getHeight(); row++) {
            String[] cells = new String[getWidth() - 'A' + 1];
            for (char col = 'A'; col <= getWidth(); col++) {
                String coord = String.valueOf(col) + row;
                String cellValue = rows.get(row - 1)[col - 'A'];

                cells[col - 'A'] = mapCell.apply(coord, cellValue);
            }
            cols.add(cells);
        }
        return new Document(cols);
    }

    /**
     * Returns a string representation of this document formatted as CSV
     */
    public String asCSV() {
        return rows
                .stream()
                .map(row -> String.join(",", row))
                .collect(Collectors.joining("\n"));
    }

    /**
     * Parses a coordinate returning a pair of strings: column and row
     */
    private static String[] parseCoordinate(String expression) {
        Matcher matcher = Pattern.compile("([A-Z])(\\d+)").matcher(expression);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Expression does not look like a coordinate: " + expression);
        }
        return new String[]{matcher.group(1), matcher.group(2)};
    }
}
