package windfall.challenge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static windfall.challenge.ExpressionEvaluator.evaluateExpression;

public class DocumentEvaluator {

    private final Document document;

    // Prevents unnecessarily expandedCells a cell more than once
    private final Map<String, String> expandedCells = new HashMap<>();

    public DocumentEvaluator(Document document) {
        this.document = document;
    }

    /**
     * Creates a new document whose cells are the expanded and evaluated
     * values of the provided {@link #document}.
     * <p>
     * The strategy is simple: recursively expand the cell as to make
     * it just literals and operators, and then use {@link ExpressionEvaluator}
     * to evaluate the expression.
     */
    public Document evaluate() {
        return document.map((coord, cell) -> {
            String expandedCell = expandCell(document, cell, new HashSet<>());
            expandedCells.put(coord, expandedCell);
            return evaluateExpression(expandedCell);
        });
    }

    private String expandCell(Document document, String cell, Set<String> cyclicDependencyDetection) {
        // Split cell value into expressions to be evaluated
        String[] expressions = ExpressionEvaluator.splitExpression(cell);

        for (int i = 0; i < expressions.length; i++) {
            if (looksLikeReference(expressions[i])) {
                String dereference = dereference(document, expressions[i], cyclicDependencyDetection);
                expressions[i] = dereference;
            } else if (!expressions[i].matches("[+-]|(\\d+(.\\d+)?)")) {
                // If expression is not a reference, make sure it is either a literal or an operator
                throw new IllegalStateException("Invalid expression: expected +, - or literal, but got " + expressions[i]);
            }
        }

        return String.join("", expressions);
    }

    private String dereference(Document document, String expression, Set<String> cyclicDependencyDetection) {
        if (expandedCells.containsKey(expression)) {
            // already expanded, just use it
            return expandedCells.get(expression);
        }

        if (!cyclicDependencyDetection.add(expression)) {
            throw new IllegalArgumentException("Circular dependency detected: " + expression);
        }
        return expandCell(document, document.getCellAt(expression), cyclicDependencyDetection);
    }

    private static boolean looksLikeReference(String expression) {
        return expression.matches("[A-Z]\\d+");
    }
}
