package windfall.challenge;

public class ExpressionEvaluator {
    /**
     * Splits a expression into operands and operators
     */
    public static String[] splitExpression(String cell) {
        return cell.replaceAll(" ", "")
                .split("(?<=[+\\-*/])|(?=[+\\-])");
    }

    /**
     * Evaluates expression arithmetically. Returns a string
     * of the result using 2 decimals of precision.
     */
    public static String evaluateExpression(String expr) {
        String[] parts = splitExpression(expr);
        Calculator calculator = new Calculator();
        for (String part : parts) {
            calculator.push(part);
        }
        return calculator.value();
    }

    private static class Calculator {
        private double value = Double.MIN_VALUE;
        private char operator = 0; // + or - or 0 (0 meaning nothing to operate)

        private void push(String operand) {
            switch (operand) {
                case "+":
                case "-":
                    if (operator != 0) {
                        throw new IllegalStateException("Invalid expression: " + operand + ". Already had an operator: " + operator);
                    }
                    operator = operand.charAt(0);
                    break;
                default:
                    double doubleValue;
                    try {
                        doubleValue = Double.parseDouble(operand);
                    } catch (NumberFormatException e) {
                        throw new IllegalStateException("Invalid expression: expected literal but got " + operand);
                    }

                    if (operator == '+') {
                        value += doubleValue;
                        operator = 0;
                    } else if (operator == '-') {
                        value -= doubleValue;
                        operator = 0;
                    } else if (value != Double.MIN_VALUE) {
                        throw new IllegalStateException("Invalid expression: literal followed by literal");
                    } else {
                        value = doubleValue;
                    }
                    break;
            }
        }

        public String value() {
            if (operator != 0) {
                throw new IllegalStateException("Invalid expression: incomplete operation");
            }
            return String.format("%.2f", value);
        }
    }
}
