package windfall.challenge;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Spreadsheet {
    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("Usage: Spreadsheet <input_file.csv> <output_file.csv>");
            System.exit(1);
        }

        String fileName = args[0];
        File file = getFile(fileName);

        try (FileReader reader = new FileReader(file);
             FileWriter fileWriter = new FileWriter(args[1])) {
            // parse and evaluate input document
            Document document = CSVParser.parse(reader);
            Document evaluatedDocument = new DocumentEvaluator(document).evaluate();

            // write output document
            fileWriter.write(evaluatedDocument.asCSV());
            fileWriter.write("\n");
            fileWriter.flush();
        }
    }

    private static File getFile(String fileName) {
        File file = new File(fileName);
        if (!file.exists()) {
            System.out.println("Invalid file: " + fileName);
            System.exit(1);
        }

        if (!file.isFile()) {
            System.out.println("Invalid file type: " + fileName);
            System.exit(1);
        }

        return file;
    }
}
