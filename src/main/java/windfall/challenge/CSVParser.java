package windfall.challenge;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVParser {
    /**
     * Parses the provided CSV data as a {@link Document}
     */
    public static Document parse(Reader reader) {
        Scanner scanner = new Scanner(reader);
        List<String[]> rows = new ArrayList<>();
        while (scanner.hasNext()) {
            String[] cells = scanner.nextLine().split(",");
            rows.add(cells);
        }
        return new Document(rows);
    }
}
