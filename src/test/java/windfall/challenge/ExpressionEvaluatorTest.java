package windfall.challenge;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static windfall.challenge.ExpressionEvaluator.evaluateExpression;
import static windfall.challenge.ExpressionEvaluator.splitExpression;

public class ExpressionEvaluatorTest {
    @Test
    public void testSplitExpressions() {
        assertThat(splitExpression("A + B - C")).containsExactly("A", "+", "B", "-", "C");
        assertThat(splitExpression("42 + B2 - 5.66")).containsExactly("42", "+", "B2", "-", "5.66");
    }

    @Test
    public void shouldEvaluateValidExpressions() {
        assertThat(evaluateExpression("42+35")).isEqualTo("77.00");
        assertThat(evaluateExpression("42-35")).isEqualTo("7.00");
        assertThat(evaluateExpression("42-35+100")).isEqualTo("107.00");
        assertThat(evaluateExpression("42-35.5+100.5")).isEqualTo("107.00");
        assertThat(evaluateExpression("-42")).isEqualTo("-42.00");
        assertThat(evaluateExpression("+33")).isEqualTo("33.00");
    }

    @Test
    public void shouldFailWithInvalidExpressions() {
        try {
            evaluateExpression("42++35");
            fail("Should have failed");
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo(("Invalid expression: +. Already had an operator: +"));
        }

        try {
            evaluateExpression("42-");
            fail("Should have failed");
        } catch (Exception e) {
            assertThat(e).hasMessage("Invalid expression: incomplete operation");
        }
    }
}
