package windfall.challenge;

import org.junit.Test;

import java.io.StringReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class TestCSVParser {
    @Test
    public void shouldParseValidDocument() {
        String contents = "B2+2,A1+A2\nB2-3,7+5";
        Document document = CSVParser.parse(new StringReader(contents));

        assertThat(document.getWidth()).isEqualTo('B');
        assertThat(document.getHeight()).isEqualTo(2);

        assertThat(document.getCellAt("A1")).isEqualTo("B2+2");
        assertThat(document.getCellAt("A2")).isEqualTo("B2-3");
        assertThat(document.getCellAt("B1")).isEqualTo("A1+A2");
        assertThat(document.getCellAt("B2")).isEqualTo("7+5");

        try {
            document.getCellAt("C1");
            fail("It should have failed to access C1");
        } catch (IllegalArgumentException e) {
            // should reach this
        }

        // serialized document should be equal to the version parsed
        assertThat(document.asCSV()).isEqualTo(contents);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToParseEmpty() {
        CSVParser.parse(new StringReader(""));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToParseWhenNoMatchingCols() {
        CSVParser.parse(new StringReader("A,B\nC"));
    }
}
