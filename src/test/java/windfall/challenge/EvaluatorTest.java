package windfall.challenge;

import org.junit.Test;

import java.io.StringReader;

import static org.assertj.core.api.Assertions.assertThat;

public class EvaluatorTest {
    @Test
    public void shouldEvaluateValidDocument() {
        String contents = "B2+2,A1+A2\nB2-3,7+5\n";
        Document document = CSVParser.parse(new StringReader(contents));

        Document evaluatedDocument = new DocumentEvaluator(document).evaluate();
        assertThat(evaluatedDocument.getCellAt("A1")).isEqualTo("14.00");
        assertThat(evaluatedDocument.getCellAt("A2")).isEqualTo("9.00");
        assertThat(evaluatedDocument.getCellAt("B1")).isEqualTo("23.00");
        assertThat(evaluatedDocument.getCellAt("B2")).isEqualTo("12.00");
    }

    @Test
    public void shouldDetectCyclicDependency() {
        String contents = "B2+2,A1+A2\nB2-3,A1\n";
        Document document = CSVParser.parse(new StringReader(contents));

        try {
            new DocumentEvaluator(document).evaluate();
        } catch (Exception e) {
            assertThat(e).hasMessage("Circular dependency detected: B2");
        }
    }

    @Test
    public void shouldFailOnOutOfBounds() {
        String contents = "B2+2,A1+A2\nB2-3,C1\n";
        Document document = CSVParser.parse(new StringReader(contents));

        try {
            new DocumentEvaluator(document).evaluate();
        } catch (Exception e) {
            assertThat(e).hasMessage("Cell out of bounds: C1");
        }
    }
}
