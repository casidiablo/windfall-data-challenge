## WindfallData Programming Assignment

This project can be built and tested using Gradle:

```
./gradlew test
```

I used Gradle for the readers' convenience. That way you can just run it from the CLI without
too much setup. Also, the assignment suggests using standard Java libraries only, but I
included `assertj` to make tests easier to read.

### Solution explanation

I tried to make things as simple as possible while still supporting all required features.
There are only 4 components:

- A very simple CSV parser that creates a `Document`
- `Document`, which is an immutable representation of a CSV file
- `DocumentEvaluator` which has the logic to evaluate the spreadsheet (by creating a new document)
- `ExpressionEvaluator` used to evaluate arithmetic expressions

Highlights:

- Ever since I've been doing functional programming (mainly in Scala and Clojure), I've learned to
  apply some of the best practices even when writing Java (e.g. favoring immutability, minimizing
  the use of state, etc.).
- In the same vein, `Document` evolved in a way that it offers a functional interface to create
  new documents using a BiFunction. Since I'm using Java 8, it made things a bit cleaner and also
  hid the complexity of traversing the document into the document class itself.
- `ExpressionEvaluator` was written in the most naive and simple way possible. Again, I was trying
  to align to the spec, but to support new operators and their precedence it would probably need a full rewrite.
- All core functionality + edge cases are covered by unit tests